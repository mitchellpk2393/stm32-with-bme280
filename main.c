/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "ssd1306.h"
#include "fonts.h"
#include "stdio.h"
#include <stdint.h>
#include <stdbool.h>
/* Private includes ----------------------------------------------------------*/


/* Private variables ---------------------------------------------------------*/
I2C_HandleTypeDef hi2c2;

RTC_HandleTypeDef hrtc;

SPI_HandleTypeDef hspi2;

UART_HandleTypeDef huart1;

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_I2C2_Init(void);
static void MX_SPI2_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_RTC_Init(void);


/* Private user code ---------------------------------------------------------*/
enum {
  BME280_REGISTER_DIG_T1 = 0x88,
  BME280_REGISTER_DIG_T2 = 0x8A,
  BME280_REGISTER_DIG_T3 = 0x8C,

  BME280_REGISTER_DIG_P1 = 0x8E,
  BME280_REGISTER_DIG_P2 = 0x90,
  BME280_REGISTER_DIG_P3 = 0x92,
  BME280_REGISTER_DIG_P4 = 0x94,
  BME280_REGISTER_DIG_P5 = 0x96,
  BME280_REGISTER_DIG_P6 = 0x98,
  BME280_REGISTER_DIG_P7 = 0x9A,
  BME280_REGISTER_DIG_P8 = 0x9C,
  BME280_REGISTER_DIG_P9 = 0x9E,

  BME280_REGISTER_DIG_H1 = 0xA1,
  BME280_REGISTER_DIG_H2 = 0xE1,
  BME280_REGISTER_DIG_H3 = 0xE3,
  BME280_REGISTER_DIG_H4 = 0xE4,
  BME280_REGISTER_DIG_H5 = 0xE5,
  BME280_REGISTER_DIG_H6 = 0xE7,

  BME280_REGISTER_CHIPID = 0xD0,
  BME280_REGISTER_VERSION = 0xD1,
  BME280_REGISTER_SOFTRESET = 0xE0,

  BME280_REGISTER_CAL26 = 0xE1, // R calibration stored in 0xE1-0xF0

  BME280_REGISTER_CONTROLHUMID = 0xF2,
  BME280_REGISTER_STATUS = 0XF3,
  BME280_REGISTER_CONTROL = 0xF4,
  BME280_REGISTER_CONFIG = 0xF5,
  BME280_REGISTER_PRESSUREDATA = 0xF7,
  BME280_REGISTER_TEMPDATA = 0xFA,
  BME280_REGISTER_HUMIDDATA = 0xFD
};
struct bme280_calib_data {
  uint16_t dig_T1; ///< temperature compensation value
  int16_t dig_T2;  ///< temperature compensation value
  int16_t dig_T3;  ///< temperature compensation value

  uint16_t dig_P1; ///< pressure compensation value
  int16_t dig_P2;  ///< pressure compensation value
  int16_t dig_P3;  ///< pressure compensation value
  int16_t dig_P4;  ///< pressure compensation value
  int16_t dig_P5;  ///< pressure compensation value
  int16_t dig_P6;  ///< pressure compensation value
  int16_t dig_P7;  ///< pressure compensation value
  int16_t dig_P8;  ///< pressure compensation value
  int16_t dig_P9;  ///< pressure compensation value

  uint16_t dig_H1; ///< humidity compensation value
  int16_t dig_H2; ///< humidity compensation value
  uint16_t dig_H3; ///< humidity compensation value
  int16_t dig_H4; ///< humidity compensation value
  int16_t dig_H5; ///< humidity compensation value
  int16_t dig_H6;  ///< humidity compensation value
};

/**
  * @brief  The application entry point.
  * @retval int
  */
uint8_t spitx[2],spirx[2] = {0};
uint8_t Readreg(uint8_t reg);
uint16_t Readreg16(uint8_t reg);
uint8_t Writereg(uint8_t reg,uint8_t value);
float bme280_readTemperature(int32_t adc_T);
int32_t Readreg24(uint8_t reg);
uint16_t read16_LE(uint8_t reg);
int16_t readS16_LE(uint8_t reg);
struct bme280_calib_data calib_reg;
bool isReadingCalibration(void);
float bme280_readPressure(int32_t adc_P);
float bme280_readHumidity(int32_t adc_H);
int32_t t_fine;
int32_t tRaw, pRaw, hRaw;
bool readbme280(void);
bool Trimread(void);
bool Trimread_H(void);
bool bme280Init(void);
bool USBsend(char send_temp[], char send_press[],char send_humi[]);
RTC_TimeTypeDef sTime = {0};
RTC_DateTypeDef sDate = {0};
RTC_AlarmTypeDef sAlarm = {0};

int main(void)
{
  
  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();
  /* Configure the system clock */
  SystemClock_Config();
  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C2_Init();
  MX_SPI2_Init();
  MX_USART1_UART_Init();
  MX_RTC_Init();
  
	while(!bme280Init());
	
	


//for(volatile uint8_t i = 0; i < 255;i++)
//	{
//		if(HAL_I2C_IsDeviceReady(&hi2c2,i,1,10) == HAL_OK)
//		{
//			HAL_GPIO_TogglePin(GPIOC,GPIO_PIN_9);
//			break;
//		}
//	}

//	if(SSD1306_Init())
//	{	
//		HAL_GPIO_WritePin(GPIOC,GPIO_PIN_8,GPIO_PIN_SET);
//	
//	}
	char USBrx[50] = {0};
	while(!SSD1306_Init());
	char send_buffer[50] = {0};
	char send_temp[50] = {0};
	char send_press[50] = {0};
	char send_humi[50] ={0};
	volatile uint32_t  times = 0;
	
	float roomtemp,roompress,roomhumi;
  while (1)
  {
    /* USER CODE END WHILE */
		readbme280();
		roomtemp = bme280_readTemperature(tRaw);	
		roompress = bme280_readPressure(pRaw)/100;
		roomhumi = bme280_readHumidity(hRaw);
		
		//SSD1306_Clear();
		sprintf(send_temp,"tempC=%0.2f",roomtemp);
		SSD1306_GotoXY(0,0);
		SSD1306_Puts(send_temp,&Font_7x10,1);
		SSD1306_GotoXY(0,18);
		sprintf(send_press,"PressHpa=%0.1f",roompress);
		SSD1306_Puts(send_press,&Font_7x10,1);
		SSD1306_GotoXY(0,30);
		sprintf(send_humi,"Humi=%0.2f",roomhumi);
		SSD1306_Puts(send_humi,&Font_7x10,1);
		HAL_RTC_GetTime(&hrtc,&sTime,RTC_FORMAT_BIN);
		HAL_RTC_GetDate(&hrtc,&sDate,RTC_FORMAT_BIN);
		
		USBsend(send_temp,send_press,send_humi);

		
		//HAL_Delay(4500);
		memset(send_buffer,0,50);
		sprintf(send_buffer,"time=%d:%d:%d",sTime.Hours,sTime.Minutes,sTime.Seconds);
		SSD1306_GotoXY(0,40);
		SSD1306_Puts(send_buffer,&Font_7x10,1);
		SSD1306_UpdateScreen();
		
		HAL_Delay(50);
    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}
bool bme280Init(void){
	uint8_t status = 0;
	uint8_t regvalue = 0;
	uint8_t bme280_id = 0x60;
	// apply soft reset
	Writereg(BME280_REGISTER_SOFTRESET,0xB6);
	
	HAL_Delay(100);
	
	while(isReadingCalibration());
	if(Readreg(0xD0) != bme280_id){
		return false;
	}
	
	// get alle coefficient temp,press, hum
	status = Trimread();
	if(status){return false;}
	HAL_Delay(10);
	uint8_t ctrl_hum =   0x03 ; 
	regvalue = Writereg(BME280_REGISTER_CONTROLHUMID,ctrl_hum);
	
	spirx[0]= Readreg(BME280_REGISTER_CONTROLHUMID);
	if(spirx[0] != ctrl_hum){return false;}
	
	uint8_t config =  (0x00 << 5) | (0x04 <<2) ;
	Writereg(BME280_REGISTER_CONFIG,config); // fillter set 4x standyby 250ms;
	spirx[0]= Readreg(BME280_REGISTER_CONFIG);
	if(spirx[0] != config){return false;}
	
	uint8_t ctrl = (0x02 <<5) |(0x05 << 2) | 0x03;
  Writereg(BME280_REGISTER_CONTROL,ctrl); // temp sample 4x , pressure sample 4x devicemoce normaal
	spirx[0]= Readreg(BME280_REGISTER_CONTROL);
	if(spirx[0] != ctrl){return false;}
	HAL_Delay(50);
	
	return true;
}
bool USBsend(char send_temp[], char send_press[],char send_humi[]){
		bool status;
		strcat(send_temp, "\n");
		strcat(send_press, "\n");
		strcat(send_humi, "\n");
		HAL_UART_Transmit(&huart1,(uint8_t *)send_temp,strlen(send_temp),10);
		HAL_UART_Transmit(&huart1,(uint8_t *)send_press,strlen(send_press),10);
		HAL_UART_Transmit(&huart1,(uint8_t *)send_humi,strlen(send_humi),10);
	
	return true;

}
bool Trimread(void){
	uint8_t trimdata[27] = {0};
	uint8_t txbuff[1];
	uint8_t status;
	// make a new input function for read data size count = cnt
	HAL_GPIO_WritePin(GPIOC,GPIO_PIN_6,GPIO_PIN_RESET);
	txbuff[0] = 0x88  | 0x80 ;// read byte added
	// transmit to read reg
	status = HAL_SPI_TransmitReceive(&hspi2,txbuff,trimdata,27,1000);
	while( hspi2.State == HAL_SPI_STATE_BUSY ) {};
	HAL_GPIO_WritePin(GPIOC,GPIO_PIN_6,GPIO_PIN_SET);
	HAL_Delay(5); //i can't find a buffer flushing command
	if (status != HAL_OK)
    {
    	//-1 returned as failure
    	return false;
    }
	calib_reg.dig_T1 = (trimdata[2]<<8) | trimdata[1];
	calib_reg.dig_T2 = (trimdata[4]<<8) | trimdata[3];
	calib_reg.dig_T3 = (trimdata[6]<<8) | trimdata[5];
	calib_reg.dig_P1 = (trimdata[8]<<8) | trimdata[7];
	calib_reg.dig_P2 = (trimdata[10]<<8) | trimdata[9];
	calib_reg.dig_P3 = (trimdata[12]<<8) | trimdata[11];
	calib_reg.dig_P4 = (trimdata[14]<<8) | trimdata[13];
	calib_reg.dig_P5 = (trimdata[16]<<8) | trimdata[15];
	calib_reg.dig_P6 = (trimdata[18]<<8) | trimdata[17];
	calib_reg.dig_P7 = (trimdata[20]<<8) | trimdata[19];
	calib_reg.dig_P8 = (trimdata[22]<<8) | trimdata[21];
	calib_reg.dig_P9 = (trimdata[24]<<8) | trimdata[23];
	calib_reg.dig_H1 = trimdata[26];
	if(Trimread_H()){
		return false;
	}
	return true;
	//	calib_reg.dig_T1 = read16_LE(BME280_REGISTER_DIG_T1);
//	calib_reg.dig_T2 = readS16_LE(BME280_REGISTER_DIG_T2);
//	calib_reg.dig_T3 = readS16_LE(BME280_REGISTER_DIG_T3);
//	
//	calib_reg.dig_P1 = read16_LE(BME280_REGISTER_DIG_P1);
//	calib_reg.dig_P2 = readS16_LE(BME280_REGISTER_DIG_P2);
//	calib_reg.dig_P3 = readS16_LE(BME280_REGISTER_DIG_P3);
//	calib_reg.dig_P4 = readS16_LE(BME280_REGISTER_DIG_P4);
//	calib_reg.dig_P5 = readS16_LE(BME280_REGISTER_DIG_P5);
//	calib_reg.dig_P6 = readS16_LE(BME280_REGISTER_DIG_P6);
//	calib_reg.dig_P7 = readS16_LE(BME280_REGISTER_DIG_P7);
//	calib_reg.dig_P8 = readS16_LE(BME280_REGISTER_DIG_P8);
//	calib_reg.dig_P9 = readS16_LE(BME280_REGISTER_DIG_P9);
//	
//	calib_reg.dig_H1 = Readreg(BME280_REGISTER_DIG_H1);
//	calib_reg.dig_H2 = readS16_LE(BME280_REGISTER_DIG_H2);
//	calib_reg.dig_H3 = Readreg(BME280_REGISTER_DIG_H3);
//	calib_reg.dig_H4 = ((int8_t)Readreg(BME280_REGISTER_DIG_H4) << 4 ) |
//										  (Readreg(BME280_REGISTER_DIG_H4 + 1) & 0xF );
//  calib_reg.dig_H5 = ((int8_t)Readreg(BME280_REGISTER_DIG_H5+1) << 4) |
//										  (Readreg(BME280_REGISTER_DIG_H5) >> 4 );
//	calib_reg.dig_H6 = (int8_t)(Readreg(BME280_REGISTER_DIG_H6));


}
bool Trimread_H(void){
	uint8_t trimdata[9] = {0};
	uint8_t txbuff[1];
	uint8_t status;
	// make a new input function for read data size count = cnt
	HAL_GPIO_WritePin(GPIOC,GPIO_PIN_6,GPIO_PIN_RESET);
	//// Read NVM from 0xE1 to 0xE7
	txbuff[0] = 0xE1  | 0x80 ;// read byte added
	// transmit to read reg
	status = HAL_SPI_TransmitReceive(&hspi2,txbuff,trimdata,8,1000);
	while( hspi2.State == HAL_SPI_STATE_BUSY ) {};
	
	HAL_GPIO_WritePin(GPIOC,GPIO_PIN_6,GPIO_PIN_SET);
	HAL_Delay(5); //i can't find a buffer flushing command
	if (status != HAL_OK)
    {
    	//-1 returned as failure
    	return false;
    }
	calib_reg.dig_H2 = (trimdata[2]<<8) | trimdata[1];
	calib_reg.dig_H3 = (trimdata[3]);
  calib_reg.dig_H4 = (trimdata[4]<<4) | (trimdata[5] & 0x0f);
	calib_reg.dig_H5 = (trimdata[6]<<4) | (trimdata[5]>>4);
	calib_reg.dig_H6 = (trimdata[7]);
	//calib_reg.dig_H1 = Readreg(BME280_REGISTER_DIG_H1);
	return true;

}
bool isReadingCalibration(void) {
	uint8_t result;
  uint8_t const rStatus = Readreg(BME280_REGISTER_STATUS);
	return  (rStatus & (1<<0)) != 0; 
}
uint8_t Writereg(uint8_t reg,uint8_t value){
	uint8_t bufftx[2];
	uint8_t status;
 //spi transmit bring cs pin to low
	HAL_GPIO_WritePin(GPIOC,GPIO_PIN_6,GPIO_PIN_RESET);
// 2. transmit to reg
	bufftx[0] = reg & 0x7F;
	bufftx[1] = value;
	status = HAL_SPI_Transmit(&hspi2,bufftx,2,100);
	while( hspi2.State == HAL_SPI_STATE_BUSY ) {};
	HAL_GPIO_WritePin(GPIOC,GPIO_PIN_6,GPIO_PIN_SET);
  // 3. bring cs pin to high
	if(status != HAL_OK)
	{
		return -1;
	}

	//HAL_GPIO_WritePin(GPIOC,GPIO_PIN_6,GPIO_PIN_SET);
	return 0;

}
uint8_t Readreg(uint8_t reg){
	uint8_t txbuff[1];
	uint8_t rxbuff[2] = {0};
	uint8_t status;
	// make a new input function for read data size count = cnt
	
	HAL_GPIO_WritePin(GPIOC,GPIO_PIN_6,GPIO_PIN_RESET);
	txbuff[0] = reg | 0x80 ;// read byte added
	// transmit to read reg
	status = HAL_SPI_TransmitReceive(&hspi2,txbuff,rxbuff,2,100);
	while( hspi2.State == HAL_SPI_STATE_BUSY ) {};
	// 3. bring cs pin to high
	HAL_GPIO_WritePin(GPIOC,GPIO_PIN_6,GPIO_PIN_SET);
	//HAL_SPI_Transmit(&hspi2,txbuff,1,200);
	// read the data
	//HAL_SPI_Receive(&hspi2,rxbuff,1,200);
	HAL_Delay(5); //i can't find a buffer flushing command
	if (status != HAL_OK)
    {
    	//-1 returned as failure
    	return -1;
    }
	
	return rxbuff[1];

}
bool readbme280(void){
	uint8_t RawData[9] = {0};
	uint8_t txbuff[1];
	uint8_t status;
	// make a new input function for read data size count = cnt
	
	HAL_GPIO_WritePin(GPIOC,GPIO_PIN_6,GPIO_PIN_RESET);
	txbuff[0] = 0xF7  | 0x80 ;// read byte added
	// transmit to read reg
	status = HAL_SPI_TransmitReceive(&hspi2,txbuff,RawData,9,500);
	while( hspi2.State == HAL_SPI_STATE_BUSY ) {};
	// 3. bring cs pin to high
	HAL_GPIO_WritePin(GPIOC,GPIO_PIN_6,GPIO_PIN_SET);
	HAL_Delay(5); //i can't find a buffer flushing command
	if (status != HAL_OK)
    {
    	//-1 returned as failure
    	return false;
    }
	pRaw = (RawData[1]<<12)|(RawData[2]<<4)|(RawData[3]>>4);
	tRaw = (RawData[4]<<12)|(RawData[5]<<4)|(RawData[6]>>4);
	hRaw = (RawData[7]<<8)|(RawData[8]);
	
	return true;


}
uint16_t read16_LE(uint8_t reg) {
  uint16_t temp = Readreg16(reg);
	temp = (temp >> 8) | (temp <<8);
	return temp;
  //return (temp >> 8) | (temp << 8);
}
int16_t readS16_LE(uint8_t reg) {
  return (int16_t)read16_LE(reg);
}
uint16_t Readreg16(uint8_t reg){
	uint8_t txbuff[1];
	uint8_t rxbuff[2]= {0};
	
	HAL_GPIO_WritePin(GPIOC,GPIO_PIN_6,GPIO_PIN_RESET);
	txbuff[0] = reg | 0x80 ;// read byte added
	// transmit to read reg
	HAL_SPI_Transmit(&hspi2,txbuff,1,100);
	// read the data
	HAL_SPI_Receive(&hspi2,rxbuff,2,100);
	uint16_t result = (uint16_t)(rxbuff[1]) << 8 | (uint16_t)(rxbuff[0]);
	// 3. bring cs pin to high
	HAL_GPIO_WritePin(GPIOC,GPIO_PIN_6,GPIO_PIN_SET);
	
	return result;

}
int32_t Readreg24(uint8_t reg){
	

	uint8_t txbuff[1];
	uint8_t rxbuff[4] =  {0};
	uint8_t status;
	
	HAL_GPIO_WritePin(GPIOC,GPIO_PIN_6,GPIO_PIN_RESET);
	txbuff[0] = reg | 0x80 ;// read byte added
	
	// transmit to read reg
	HAL_SPI_Transmit(&hspi2,txbuff,1,1000);
	// read the data
	status = HAL_SPI_Receive(&hspi2,rxbuff,3,1000);
	// 3. bring cs pin to high
	//HAL_GPIO_WritePin(GPIOC,GPIO_PIN_6,GPIO_PIN_SET);
	//status = HAL_SPI_TransmitReceive(&hspi2,txbuff,rxbuff,4,100);
	while( hspi2.State == HAL_SPI_STATE_BUSY ) {};
	HAL_GPIO_WritePin(GPIOC,GPIO_PIN_6,GPIO_PIN_SET);
	HAL_Delay(5);
	if (status != HAL_OK)
	{
		//-1 returned as failure
		return -1;
	}
		
		
	// transmit to read reg
	//HAL_SPI_Transmit(&hspi2,txbuff,1,1000);
	// read the data
	//HAL_SPI_Receive(&hspi2,rxbuff,3,1000);
	// 3. bring cs pin to high
	//HAL_GPIO_WritePin(GPIOC,GPIO_PIN_6,GPIO_PIN_SET);
//	int32_t result = (int32_t)(rxbuff[0] << 16) | (int32_t)(rxbuff[1]<< 8) |
//         (int32_t)(rxbuff[2]);
//	return result;
	return (uint32_t)(rxbuff[0] << 12) | (uint32_t)(rxbuff[1]<< 4) |
         (uint32_t)(rxbuff[2])>>4;

}


float bme280_readTemperature(int32_t adc_T) {
	
  int32_t var1, var2,var3;
	

  //int32_t adc_T2 = Readreg24(BME280_REGISTER_TEMPDATA);
  if (adc_T == 0x800000)
		{
			return -1;
		} // value in case temp measurement was disabled

//  var1 = (int32_t)((adc_T >> 3) - ((int32_t)calib_reg.dig_T1 << 1));
//  var1 = (var1 * ((int32_t)calib_reg.dig_T2)) >> 11;
//  var2 = (((((adc_T>>4) - ((int32_t)calib_reg.dig_T1)) * ((adc_T>>4) - ((int32_t)calib_reg.dig_T1)))>> 12) *((int32_t)calib_reg.dig_T3)) >> 14;

//  t_fine = var1 + var2;

//  var3 = (t_fine * 5 + 128) >> 8;
	var1 = (int32_t)((adc_T / 8) - ((int32_t)calib_reg.dig_T1 * 2));
  var1 = (var1 * ((int32_t)calib_reg.dig_T2)) / 2048;
  var2 = (int32_t)((adc_T / 16) - ((int32_t)calib_reg.dig_T1));
  var2 = (((var2 * var2) / 4096) * ((int32_t)calib_reg.dig_T3)) / 16384;

  t_fine = var1 + var2;

  int32_t T = (t_fine * 5 + 128) / 256;

  return (float)T / 100;
	
  //return var3;
}

float bme280_readPressure(int32_t adc_P){
	int64_t var1, var2, var3, var4;


  //int32_t adc_P = Readreg(BME280_REGISTER_PRESSUREDATA);
  if (adc_P == 0x800000)
	{
			return -1;
	}		// value in case pressure measurement was disabled
  

  var1 = ((int64_t)t_fine) - 128000;
  var2 = var1 * var1 * (int64_t)calib_reg.dig_P6;
  var2 = var2 + ((var1 * (int64_t)calib_reg.dig_P5) <<17);
  var2 = var2 + (((int64_t)calib_reg.dig_P4) <<35);
  var1 = ((var1 * var1 * (int64_t)calib_reg.dig_P3) / 256) +
         ((var1 * ((int64_t)calib_reg.dig_P2) * 4096));
  //var3 = ((int64_t)1) * 140737488355328;
  //var1 = (var3 + var1) * ((int64_t)calib_reg.dig_P1) / 8589934592;
	var1 = (((((int64_t)1)<<47)+var1))  * ((int64_t)calib_reg.dig_P1)>>33;
  if (var1 == 0) {
    return -1; // avoid exception caused by division by zero
  }

  var4 = 1048576 - adc_P;
  var4 = (((var4 * 2147483648) - var2) * 3125) / var1;
  var1 = (((int64_t)calib_reg.dig_P9) * (var4 / 8192) * (var4 / 8192)) /
         33554432;
  var2 = (((int64_t)calib_reg.dig_P8) * var4) / 524288;
  var4 = ((var4 + var1 + var2) / 256) + (((int64_t)calib_reg.dig_P7) * 16);

  float P = var4 / 256.0;

  return P;
}


float bme280_readHumidity(int32_t adc_H){
	int32_t var1, var2, var3, var4, var5;

  //int32_t adc_H = Readreg16(BME280_REGISTER_HUMIDDATA);
  if (adc_H == 0x8000)
	{
		return -1;
	}		// value in case humidity measurement was disabled
    

	int32_t v_x1_u32r;
	v_x1_u32r = (t_fine - ((int32_t)76800));
	v_x1_u32r = (((((adc_H << 14) - (((int32_t)calib_reg.dig_H4) << 20) - (((int32_t)calib_reg.dig_H5) *\
			v_x1_u32r)) + ((int32_t)16384)) >> 15) * (((((((v_x1_u32r *\
					((int32_t)calib_reg.dig_H6)) >> 10) * (((v_x1_u32r * ((int32_t)calib_reg.dig_H3)) >> 11) +\
							((int32_t)32768))) >> 10) + ((int32_t)2097152)) * ((int32_t)calib_reg.dig_H2) +\
					8192) >> 14));
	v_x1_u32r = (v_x1_u32r - (((((v_x1_u32r >> 15) * (v_x1_u32r >> 15)) >> 7) *\
			((int32_t)calib_reg.dig_H1)) >> 4));
	v_x1_u32r = (v_x1_u32r < 0 ? 0 : v_x1_u32r);
	v_x1_u32r = (v_x1_u32r > 419430400 ? 419430400 : v_x1_u32r);                // (uint32_t)(var5 / 4096);
	//float H = (uint32_t)(v_x1_u32r>>12);
  return ((uint32_t)(v_x1_u32r>>12)/ 1024.0);


}
/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_LSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_RTC;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK1;
  PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSI;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}
static void MX_RTC_Init(void)
{

  /* USER CODE BEGIN RTC_Init 0 */

  /* USER CODE END RTC_Init 0 */

  //RTC_TimeTypeDef sTime = {0};
  //RTC_DateTypeDef sDate = {0};
  //RTC_AlarmTypeDef sAlarm = {0};

  /* USER CODE BEGIN RTC_Init 1 */

  /* USER CODE END RTC_Init 1 */
  /** Initialize RTC Only
  */
  hrtc.Instance = RTC;
  hrtc.Init.HourFormat = RTC_HOURFORMAT_24;
	// lsi set to 40Khz Apre = 127 Spre = 311
  hrtc.Init.AsynchPrediv = 127;
  hrtc.Init.SynchPrediv = 311;
  hrtc.Init.OutPut = RTC_OUTPUT_DISABLE;
  hrtc.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
  hrtc.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;
  if (HAL_RTC_Init(&hrtc) != HAL_OK)
  {
    Error_Handler();
  }

  /* USER CODE BEGIN Check_RTC_BKUP */

  /* USER CODE END Check_RTC_BKUP */

  /** Initialize RTC and set the Time and Date
  */
  sTime.Hours = 19;
  sTime.Minutes = 02;
  sTime.Seconds = 45;
  sTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
  sTime.StoreOperation = RTC_STOREOPERATION_RESET;
  if (HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BIN) != HAL_OK)
  {
    Error_Handler();
  }
  sDate.WeekDay = RTC_WEEKDAY_TUESDAY;
  sDate.Month = RTC_MONTH_JANUARY;
  sDate.Date = 18;
  sDate.Year = 22;

  if (HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BIN) != HAL_OK)
  {
    Error_Handler();
  }
  /** Enable the Alarm A
  */
  sAlarm.AlarmTime.Hours = 0;
  sAlarm.AlarmTime.Minutes = 0;
  sAlarm.AlarmTime.Seconds = 10;
  sAlarm.AlarmTime.SubSeconds = 0;
  sAlarm.AlarmTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
  sAlarm.AlarmTime.StoreOperation = RTC_STOREOPERATION_RESET;
  sAlarm.AlarmMask = RTC_ALARMMASK_NONE;
  sAlarm.AlarmSubSecondMask = RTC_ALARMSUBSECONDMASK_ALL;
  sAlarm.AlarmDateWeekDaySel = RTC_ALARMDATEWEEKDAYSEL_WEEKDAY;
  sAlarm.AlarmDateWeekDay = RTC_WEEKDAY_SUNDAY;
  sAlarm.Alarm = RTC_ALARM_A;
  if (HAL_RTC_SetAlarm_IT(&hrtc, &sAlarm, RTC_FORMAT_BIN) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN RTC_Init 2 */

  /* USER CODE END RTC_Init 2 */

}
/**
  * @brief I2C2 Initialization Function
  */
static void MX_I2C2_Init(void)
{

  /* USER CODE BEGIN I2C2_Init 0 */

  /* USER CODE END I2C2_Init 0 */

  /* USER CODE BEGIN I2C2_Init 1 */

  /* USER CODE END I2C2_Init 1 */
  hi2c2.Instance = I2C2;
  hi2c2.Init.Timing = 0x0000020B;
  hi2c2.Init.OwnAddress1 = 0;
  hi2c2.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c2.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c2.Init.OwnAddress2 = 0;
  hi2c2.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c2.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c2.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c2) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Analogue filter
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c2, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Digital filter
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c2, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C2_Init 2 */

  /* USER CODE END I2C2_Init 2 */

}

/**
  * @brief SPI2 Initialization Function
  
  */
static void MX_SPI2_Init(void)
{

  /* SPI2 parameter configuration*/
  hspi2.Instance = SPI2;
  hspi2.Init.Mode = SPI_MODE_MASTER;
  hspi2.Init.Direction = SPI_DIRECTION_2LINES;
  hspi2.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi2.Init.CLKPolarity = SPI_POLARITY_HIGH;
  hspi2.Init.CLKPhase = SPI_PHASE_2EDGE;
  hspi2.Init.NSS = SPI_NSS_SOFT;
  hspi2.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_8;
  hspi2.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi2.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi2.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi2.Init.CRCPolynomial = 10;
  hspi2.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
  hspi2.Init.NSSPMode = SPI_NSS_PULSE_ENABLE;
  if (HAL_SPI_Init(&hspi2) != HAL_OK)
  {
    Error_Handler();
  }
  

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_6, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8|GPIO_PIN_9, GPIO_PIN_RESET);

  /*Configure GPIO pins : PC6 PC8 PC9 */
  GPIO_InitStruct.Pin = GPIO_PIN_6|GPIO_PIN_8|GPIO_PIN_9;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

}


/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
